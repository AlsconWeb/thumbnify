import React, { useState } from 'react';
import style from './style.module.css';

const ImageComponent = () => {
	const [ image, setImage ] = useState();

	if ( ! image ) {
		return (
			<div className={style.image_component}>
				No image
			</div>
		);
	}

	return (
		<div>
			Set image
		</div>
	);
};

export default ImageComponent;
