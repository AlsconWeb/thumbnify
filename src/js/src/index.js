import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './global.style.css';

const root = ReactDOM.createRoot( document.getElementById( 'tn_root_app' ) );
root.render(
	<React.StrictMode>
		<App/>
	</React.StrictMode>
);
